#!/usr/bin/python3

import os
import subprocess
import mysql.connector

# Database configuration
db_host = 'localhost'
db_user = 'admin'
db_password = 'admin_pw'

# Active Directory group name 
ad_group_name = 'group_name'

# Connect to the MariaDB server as the admin
try:
    db_conn = mysql.connector.connect(host=db_host, user=db_user, password=db_password)
    cursor = db_conn.cursor()
except mysql.connector.Error as err:
    print(f"Error connecting to the MariaDB server: {err}")
    exit(1)

# Retrieve AD group members using 'getent group' and 'cut' to make a pretty list
try:
    group_members_output = subprocess.check_output(f"getent group {ad_group_name} | cut -d: -f4", shell=True).decode('utf-8')
    ad_members = group_members_output.strip().split(',')
except subprocess.CalledProcessError as err:
    print(f"Error retrieving Active Directory group members: {err}")
    cursor.close()
    db_conn.close()
    exit(1)

## Do not uncomment unless you really know what you are doing .... 
## Uncomment here to check if users have been removed from the AD group 
## Get the list of existing MariaDB users
#cursor.execute("SELECT User FROM mysql.user")
#existing_users = [user[0] for user in cursor.fetchall()]

## Remove users who are no longer in the Active Directory group
#for user in existing_users:
#    if user not in ad_members:
#        try:
#            # Revoke all privileges for the user
#            cursor.execute(f"REVOKE ALL PRIVILEGES ON *.* FROM '{user}'@'localhost'")

#            # Remove the user
#            cursor.execute(f"DROP USER '{user}'@'localhost'")

#            print(f"User '{user}' removed successfully.")
#        except mysql.connector.Error as err:
#            print(f"Error removing user '{user}': {err}")


# Create a database for each user using their AD name as the DB name, and grant full access
for username in ad_members:
    try:
        # Check if the user already exists
        cursor.execute(f"SELECT 1 FROM mysql.user WHERE User = '{username}'")
        user_exists = cursor.fetchone()

        if user_exists:
            print(f"User '{username}' already exists. Skipping user creation.")
            continue

        # Check if the database already exists
        cursor.execute(f"SELECT 1 FROM information_schema.schemata WHERE schema_name = '{username}'")
        db_exists = cursor.fetchone()

        if db_exists:
            print(f"Database '{username}' already exists. Skipping database creation.")
            continue

        # Create a new database with the AD username
        cursor.execute(f"CREATE DATABASE `{username}`")
        print(f"Database '{username}' created successfully.")

        # Create the new user with 'unix_socket' authentication, this is their AD password
        cursor.execute(f"CREATE USER '{username}'@'localhost' IDENTIFIED VIA unix_socket")
        print(f"User '{username}' created successfully.")

        # Grant full privileges to the user for their own database
        cursor.execute(f"GRANT ALL PRIVILEGES ON `{username}`.* TO '{username}'@'localhost'")
        print(f"Privileges granted to user '{username}' for database '{username}'.")
    except mysql.connector.Error as err:
        print(f"Error creating database and user for '{username}': {err}")

# Commit the changes and close the connection
db_conn.commit()
cursor.close()
db_conn.close()
