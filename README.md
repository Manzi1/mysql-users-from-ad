# MySQL Users from AD

Python script that reads and generates a list of users from an AD group, checks list against pre-existing MySQL users and creates any that do not already exist. 

Creates new users using `unix_socket` authentication, meaning AD credentials can be used rather than requiring a separate MySQL password. 

Script will create the user a database using their name and give them full access to their database.
